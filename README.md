# Bluecrypt JWK to SSH (for Browsers)

A minimal library to parse an SSH public key (`id_rsa.pub`)
and convert it into a public JWK using Vanilla JS.

Works for RSA and ECDSA public keys.

# Features

&lt; 100 lines of code | &lt; 1.0kb gzipped | 2.0kb minified | 2.9kb with comments

* [x] SSH Public Keys
* [x] RSA Public Keys
* [x] EC Public Keys
  * P-256 (prime256v1, secp256r1)
  * P-384 (secp384r1)
* [x] node.js version
  * [jwk-to-ssh.js](https://git.coolaj86.com/coolaj86/jwk-to-ssh.js)
* [x] on npm as [bluecrypt-jwk-to-ssh](https://www.npmjs.com/package/bluecrypt-jwk-to-ssh)

### Need SSH Private Keys?

SSH private keys (`id_rsa`) are just normal PEM files.

# Web Demo

<https://coolaj86.com/demos/jwk-to-ssh/>

<img border="1" src="https://i.imgur.com/LY3JGPY.png" />

```bash
git clone https://git.coolaj86.com/coolaj86/bluecrypt-jwk-to-ssh.js
pushd bluecrypt-jwk-to-ssh.js/
```

```bash
open index.html
```

# Install

You can use it as a plain-old javascript library:

```html
<script src="https://git.coolaj86.com/coolaj86/bluecrypt-jwk-to-ssh.js/raw/branch/master/jwk-to-ssh.js"></script>
```

It's also on npm:

```bash
npm install bluecrypt-jwk-to-ssh
```

# Usage

Very simple:

```js
var jwk = {
  "kty": "EC",
  "crv": "P-256",
  "x": "IT1SWLxsacPiE5Z16jkopAn8_-85rMjgyCokrnjDft4",
  "y": "mP2JwOAOdMmXuwpxbKng3KZz27mz-nKWIlXJ3rzSGMo"
};

var pub = SSH.pack({ jwk: jwk, comment: 'root@localhost' });

console.info(pub);
```

# Other Tools in the Bluecrypt Suite

* [Bluecrypt JWK to SSH](https://git.coolaj86.com/coolaj86/bluecrypt-jwk-to-ssh.js) (RSA, EC, SSH)
* [Bluecrypt ASN.1 decoder](https://git.coolaj86.com/coolaj86/asn1-parser.js) (x509, RSA, EC, etc)
* [Bluecrypt ASN.1 builder](https://git.coolaj86.com/coolaj86/asn1-packer.js) (x509, RSA, EC, etc)

# Legal

[jwk-to-ssh.js](https://git.coolaj86.com/coolaj86/jwk-to-ssh.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)

Bluecrypt&trade; is owned by AJ ONeal
